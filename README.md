# Delivery-Product Servic

A simple delivery product service.

---
The simple delivery product service has 2 seperated sections(delivery-complex).
complexes can create or cancel application and deliveries can see applications,
after that approved the applications and take it to destination.
---

The project has been doing based on service and repository.
Although you said that not needed to authentication process, but I implemented that by sanctum package.
The main classes are App/Base that you can see them such as BaseModel and ...
services typically have generated method and call form controller or other services
also you can call other methods in each service simply.
I used service container for handling some dependency injections in project.
I also use Event and job for handling some aspect of applications.
I provide some feature tests about endpoints that you can see them on /test/Feature.
Application cancel service has been implemented by Builder pattern.
The final response as a trait is based on JsonResponse that you can use it somewhere you want.
By default, a complex and delivery user have created(You can seed them on factories and use it in authentication).

---
I used **`laravel`** as a main tool to develop code. It's obviously one of the most common frameworks to produce
more efficient and shorter codes. It makes me to focus on business and the logic of project.

This project follows the `RESTFUL` best practices exposing needed endpoints

---

I hope this project will be continued in order to implement other concepts.

---
** Getting started**
---

Launch the application with sail as you can see this package has been added in new version of laravel framework

    $ composer install
    $ sail up -d
    $ sail artisan migrate --seed

    $ sail artisan migrate --seed --env=testing
    $ sail artisan test

### APIs

Method | Path                                               | Description                         |
-------|----------------------------------------------------|-------------------------------------|
POST    | /api/v1/auth/complex-login                         | Complex login                       |
POST    | /api/v1/auth/complex-login                         | Delivery login                      |
PUT   | /api/v1/auth                                       | Update Authenticated user           |
POST    | /api/v1/applications                               | Create a application instance       |
GET    | /api/v1/applications                               | Retrieve applications list          |
GET    | /api/v1/applications/{applicationId}               | Retrieve application detail         |
PUT    | /api/v1/applications/{applicationId}/cancel        | Application cancel(Complex)         |
PUT    | /api/v1/applications/{applicationId}/approved      | Application approved(Delivery)      |
PUT    | /api/v1/applications/{applicationId}/change-status | Application change status(Delivery) |

Please check it out !

** And also you can see the database tables on [http://localhost:8085)
and Login to phpmyadmin with below information
host=mysql
username=sail
password=password

---

The tools that were used in this project;

- PHP 8.2
- Laravel 10.0
- FormRequest; for validation
- Service Container; for dependency injection
- Event-Listeners ; For update product count in product model
- Sail; for containerization

---
Notice!!!
--
It should be said that I could use Redis as cache, Resources for data response,
some features such as pagination and search about api Application lists and some others tools, but
I decided to maintain it as simple as possible, so I did not add them and ignored them for this step.
---
