<?php

namespace App\Exceptions;

use Throwable;
use App\Base\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use Response;

    public function render($request, Throwable $exception): JsonResponse
    {
        return match (true) {
            $exception instanceof HttpException => $this->handleHttpException($exception),
            $exception instanceof AuthenticationException => $this->handleUnAuthenticated($exception),
            $exception instanceof NotFoundHttpException OR $exception instanceof ModelNotFoundException
                => $this->handleNotFoundException($exception),
            $exception instanceof AuthorizationException => $this->handleForbiddenException($exception),
            $exception instanceof ValidationException => $this->handleValidationException($exception),
            default => $this->handleCustomError($exception),
        };
    }

    public function handleHttpException(HttpException $exception): JsonResponse
    {
        $httpStatusCode = $this->checkHttpStatusCode($exception);

        return $this->error($httpStatusCode, HttpResponse::$statusTexts[$httpStatusCode]);
    }

    private function checkHttpStatusCode(HttpException $exception): int
    {
        if ($exception->getStatusCode() > 400 AND $exception->getStatusCode() < 500) {
            return $exception->getStatusCode();
        } else {
            return HttpResponse::HTTP_BAD_REQUEST;
        }
    }

    public function handleUnAuthenticated(AuthenticationException $exception): JsonResponse
    {
        return $this->error(
            HttpResponse::HTTP_UNAUTHORIZED,
            HttpResponse::$statusTexts[HttpResponse::HTTP_UNAUTHORIZED]
        );
    }

    public function handleNotFoundException(NotFoundHttpException|ModelNotFoundException $exception): JsonResponse
    {
        return $this->error(
            HttpResponse::HTTP_NOT_FOUND,
            HttpResponse::$statusTexts[HttpResponse::HTTP_NOT_FOUND]
        );
    }

    public function handleForbiddenException(AuthorizationException $exception): JsonResponse
    {
        return $this->error(
            HttpResponse::HTTP_FORBIDDEN,
            HttpResponse::$statusTexts[HttpResponse::HTTP_FORBIDDEN]
        );
    }

    public function handleValidationException(ValidationException $exception): JsonResponse
    {
        $errorsMessage = [];
        foreach ($exception->errors() as $errors) {
            foreach ($errors as $error) {
                $errorsMessage[] = $error;
            }
        }

        return $this->error(
            HttpResponse::HTTP_UNPROCESSABLE_ENTITY,
            HttpResponse::$statusTexts[HttpResponse::HTTP_UNPROCESSABLE_ENTITY],
            $errorsMessage
        );
    }

    public function handleCustomError(Throwable $exception): JsonResponse
    {
        dd($exception);
        return $this->error(
            HttpResponse::HTTP_INTERNAL_SERVER_ERROR,
            HttpResponse::$statusTexts[HttpResponse::HTTP_INTERNAL_SERVER_ERROR],
            env('APP_DEBUG') == false
                ? [$exception->getMessage()]
                : [HttpResponse::$statusTexts[HttpResponse::HTTP_INTERNAL_SERVER_ERROR]]

        );
    }
}
