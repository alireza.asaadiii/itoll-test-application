<?php

namespace App\Http\Controllers;

use App\Base\Response;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response as HttpResponse;

use App\Services\{ApplicationDetailService,
    ApplicationListService,
    ApplicationCancelService,
    ApplicationCreateService,
    ApplicationApprovedService,
    ApplicationChangeStatusService};

use App\Http\Requests\{
    ApplicationCreateRequest,
    ApplicationChangeStatusRequest};

class ApplicationController extends Controller
{
    use Response;

    public function create(ApplicationCreateRequest $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_CREATED,
            HttpResponse::$statusTexts[HttpResponse::HTTP_CREATED],
            (new ApplicationCreateService())->generate($request->input())
        );
    }

    public function cancelByComplex(Application $application, Request $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new ApplicationCancelService())->generate($application)
        );
    }

    public function list(Request $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new ApplicationListService())->generate()
        );
    }

    public function approved(Application $application, Request $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new ApplicationApprovedService())->generate($application)
        );
    }

    public function changeStatus(Application $application, ApplicationChangeStatusRequest $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new ApplicationChangeStatusService())->generate($application, $request->validated())
        );
    }

    public function detail(Application $application): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new ApplicationDetailService)->generate($application)
        );
    }
}
