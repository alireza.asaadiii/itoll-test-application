<?php

namespace App\Http\Controllers;

use App\Base\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Response as HttpResponse;
use App\Services\Login\{ComplexLoginService, DeliveryLoginService};

class AuthController extends Controller
{
    use Response;

    public function complexLogin(LoginRequest $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_CREATED,
            HttpResponse::$statusTexts[HttpResponse::HTTP_CREATED],
            (new ComplexLoginService())->generate($request->input())
        );
    }

    public function deliveryLogin(LoginRequest $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_CREATED,
            HttpResponse::$statusTexts[HttpResponse::HTTP_CREATED],
            (new DeliveryLoginService())->generate($request->input())
        );
    }
}
