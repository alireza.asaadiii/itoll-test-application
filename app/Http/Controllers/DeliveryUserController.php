<?php

namespace App\Http\Controllers;

use App\Base\Response;
use Illuminate\Http\JsonResponse;
use App\Services\DeliveryUserUpdateService;
use App\Http\Requests\DeliveryUserUpdateRequest;

use Illuminate\Http\Response as HttpResponse;

class DeliveryUserController extends Controller
{
    use Response;

    public function update(DeliveryUserUpdateRequest $request): JsonResponse
    {
        return $this->success(
            HttpResponse::HTTP_OK,
            HttpResponse::$statusTexts[HttpResponse::HTTP_OK],
            (new DeliveryUserUpdateService())->generate($request->validated())
        );
    }
}
