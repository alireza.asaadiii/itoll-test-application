<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'source_location_longitude' => 'required|numeric',
            'source_location_latitude' => 'required|numeric',
            'source_address' => 'required|string',
            'source_name' => 'required|string',
            'source_phone_number' => 'required|numeric',

            'destination_location_longitude' => 'required|numeric',
            'destination_location_latitude' => 'required|numeric',
            'destination_address' => 'required|string',
            'destination_name' => 'required|string',
            'destination_phone_number' => 'required|numeric',
        ];
    }
}
