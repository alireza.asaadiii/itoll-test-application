<?php

namespace App\Interfaces;

use DateTimeInterface;

interface UserInterface
{
    public function createToken(
        string $name,
        array $abilities = ['*'],
        DateTimeInterface $expiresAt = null
    );
}
