<?php

namespace App\Jobs;

use App\Models\Application;
use Illuminate\Bus\Queueable;
use App\Models\ApplicationStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\ApplicationUpdateService;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\ApplicationStatusCreateService;

class ApplicationApprovedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public Application $application)
    {

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        DB::beginTransaction();
            (new ApplicationStatusCreateService)->generate(
                $this->application,
                ['status_id' => ApplicationStatus::STATUS['approved']]
            );

            (new ApplicationUpdateService)->generate(
                $this->application,
                ['user_id' => auth()->user()->id]
            );

        DB::commit();
    }
}
