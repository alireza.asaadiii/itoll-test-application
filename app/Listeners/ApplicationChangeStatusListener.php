<?php

namespace App\Listeners;

use Exception;
use App\Models\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Events\ApplicationChangeStatusEvent;

class ApplicationChangeStatusListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(ApplicationChangeStatusEvent $event): void
    {
        try {
            foreach (auth()->user()->applications as $application) {
                Http::timeout(3)->post($application->complex->url, [
                    'application' => $application,
                    'status' => $application->lastStatus,
                    'delivery_location' => $application->user
                ]);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
