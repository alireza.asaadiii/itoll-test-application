<?php

namespace App\Models;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Relations\{HasMany, HasOne, BelongsTo};

class Application extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

    public function statuses(): HasMany
    {
        return $this->hasMany(ApplicationStatus::class, 'application_id', 'id');
    }

    public function lastStatus(): HasOne
    {
        return $this->hasOne(ApplicationStatus::class, 'application_id', 'id')
            ->latest();
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(DeliveryUser::class, 'user_id', 'id');
    }

    public function complex(): BelongsTo
    {
        return $this->belongsTo(Complex::class, 'complex_id', 'id');
    }
}
