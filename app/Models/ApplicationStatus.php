<?php

namespace App\Models;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ApplicationStatus extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'application_statuses';

    CONST STATUS = [
        'pending' => 1,
        'canceled' => 2,
        'approved' => 3,
        'received' => 4,
        'done' => 5,
    ];

    public static function fetchUnRelatedIdForApplicationList(): array
    {
        return [
            self::STATUS['approved'],
            self::STATUS['canceled'],
            self::STATUS['received'],
            self::STATUS['done'],
        ];
    }

    public function application(): BelongsTo
    {
        return $this->belongsTo(User::class, 'application_id', 'id');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
}
