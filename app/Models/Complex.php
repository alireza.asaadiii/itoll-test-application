<?php

namespace App\Models;

use App\Base\BaseModel;

class Complex extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'complexes';
}
