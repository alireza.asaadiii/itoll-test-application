<?php

namespace App\Models;

use App\Base\BaseModel;

class Status extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'statuses';
}
