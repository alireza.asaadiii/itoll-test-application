<?php

namespace App\Providers;

use App\Models\Application as ApplicationModel;
use App\Models\ApplicationLocation;
use App\Models\ApplicationStatus;
use App\Models\DeliveryUser;
use App\Repositories\ApplicationLocationRepository;
use App\Repositories\ApplicationRepository;
use App\Repositories\ApplicationStatusRepository;
use App\Repositories\DeliveryUserRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ApplicationRepository::class, function (Application $app) {
            return new ApplicationRepository($app->make(ApplicationModel::class));
        });

        $this->app->bind(ApplicationStatusRepository::class, function (Application $app) {
            return new ApplicationStatusRepository($app->make(ApplicationStatus::class));
        });

        $this->app->bind(ApplicationLocationRepository::class, function (Application $app) {
            return new ApplicationLocationRepository($app->make(ApplicationLocation::class));
        });
        $this->app->bind(DeliveryUserRepository::class, function (Application $app) {
            return new DeliveryUserRepository($app->make(DeliveryUser::class));
        });

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
