<?php

namespace App\Repositories;

use App\Models\Application;
use App\Models\ApplicationStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class ApplicationRepository
{
    private Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $applicationCreationData): Application
    {
        return $this->model->create($applicationCreationData);
    }

    public function updateUser(Application $application, array $data): int
    {
        return $this->model->lockForUpdate()
            ->where('id', $application->id)
            ->whereNull('user_id')
            ->update($data);
    }

    public function fetchList(): Collection
    {
        return $this->model::whereDoesntHave('statuses', function ($query) {
            $query->whereIn('status_id', ApplicationStatus::fetchUnRelatedIdForApplicationList());
        })->get();
    }
}
