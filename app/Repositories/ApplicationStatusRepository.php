<?php

namespace App\Repositories;

use App\Models\ApplicationStatus;
use Illuminate\Database\Eloquent\Model;

class ApplicationStatusRepository
{
    private Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $applicationCreationData): ApplicationStatus
    {
        return $this->model->firstOrCreate($applicationCreationData);
    }
}
