<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class DeliveryUserRepository
{
    private Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function update(array $data): int
    {
        return $this->model->where('id', auth()->user()->id)->update($data);
    }
}
