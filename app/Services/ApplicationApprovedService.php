<?php

namespace App\Services;

use App\Models\Application;
use App\Models\ApplicationStatus;
use App\Jobs\ApplicationApprovedJob;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ApplicationApprovedService
{
    public function generate(Application $application): Application
    {
        $application = Application::lockForUpdate()->find($application->id);

        if ($application->lastStatus->status_id != ApplicationStatus::STATUS['pending']) {
            throw new UnprocessableEntityHttpException();
        }

        ApplicationApprovedJob::dispatch($application)->delay(now()->addSecond(2));

        return $application->refresh();
    }
}
