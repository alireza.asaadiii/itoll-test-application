<?php

namespace App\Services;

use App\Models\Application;
use App\Services\Builder\ApplicationCancelBuilder;
use App\Services\Builder\ApplicationCancelDirector;
use Illuminate\Auth\Access\AuthorizationException;

class ApplicationCancelService
{
    /**
     * @throws AuthorizationException
     */
    public function generate(Application $application): array
    {
        return ApplicationCancelDirector::build(new ApplicationCancelBuilder($application));
    }
}
