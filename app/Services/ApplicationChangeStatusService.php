<?php

namespace App\Services;

use App\Models\Application;
use Illuminate\Support\Facades\DB;

class ApplicationChangeStatusService
{
    public function generate(Application $application, array $data): Application
    {
        DB::beginTransaction();
            (new ApplicationStatusCreateService)->generate($application, $data);

            unset($data['status_id']);
            (new DeliveryUserUpdateService)->generate($data);
        DB::commit();

        return $application->refresh();
    }
}
