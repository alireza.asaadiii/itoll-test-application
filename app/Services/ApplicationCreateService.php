<?php

namespace App\Services;

use App\Models\Application;
use App\Models\ApplicationStatus;
use Illuminate\Support\Facades\{DB, App};
use App\Repositories\ApplicationRepository;

class ApplicationCreateService
{
    public function generate(array $data): Application
    {
        DB::beginTransaction();
            $application = App::make(ApplicationRepository::class)
                ->create($this->prepareDataCreationService($data));

            (new ApplicationStatusCreateService)->generate(
                $application,
                ['status_id' => ApplicationStatus::STATUS['pending']]
            );
        DB::commit();

        return $application;
    }

    private function prepareDataCreationService(array $applicationCreationData): array
    {
        $applicationCreationData['creator_id'] = auth()->user()->id;
        $applicationCreationData['complex_id'] = auth()->user()->complex_id;
        $applicationCreationData['code'] = CodeGeneratorService::generate();

        return $applicationCreationData;
    }
}
