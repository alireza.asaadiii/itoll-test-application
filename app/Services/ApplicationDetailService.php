<?php

namespace App\Services;

use App\Models\Application;
use Illuminate\Auth\Access\AuthorizationException;

class ApplicationDetailService
{
    public function generate(Application $application): Application
    {
        $this->checkApplicationOwnership($application);
        return $application->with(['statuses', 'user'])->first();
    }

    private function checkApplicationOwnership(Application $application): void
    {
        if ($application->complex_id != auth()->user()->complex_id) {
            throw new AuthorizationException();
        }
    }
}
