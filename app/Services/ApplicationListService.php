<?php

namespace App\Services;

use Illuminate\Support\Facades\App;
use App\Repositories\ApplicationRepository;
use Illuminate\Database\Eloquent\Collection;

class ApplicationListService
{
    public function generate(): Collection
    {
        return App::make(ApplicationRepository::class)->fetchList();
    }
}
