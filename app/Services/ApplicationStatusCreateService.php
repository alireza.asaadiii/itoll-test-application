<?php

namespace App\Services;

use App\Models\Application;
use App\Models\ApplicationStatus;
use Illuminate\Support\Facades\App;
use App\Repositories\ApplicationStatusRepository;

class ApplicationStatusCreateService
{
    public function generate(Application $application, array $data): ApplicationStatus
    {
        return App::make(ApplicationStatusRepository::class)
            ->create($this->prepareDataCreationService($application, $data));
    }

    private function prepareDataCreationService(
        Application $application,
        array $data
    ): array {
        $applicationStatusCreationData['status_id'] = $data['status_id'];
        $applicationStatusCreationData['application_id'] = $application->id;

        return $applicationStatusCreationData;
    }
}
