<?php

namespace App\Services;

use Exception;
use App\Models\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Response as HttpResponse;
use App\Repositories\ApplicationRepository;

class ApplicationUpdateService
{
    public function generate(Application $application, array $data): void
    {
        $updateResult = App::make(ApplicationRepository::class)
            ->updateUser($application, $data);

        if (!$updateResult) {
            throw new Exception(
                HttpResponse::HTTP_UNPROCESSABLE_ENTITY,
                'A delivery has been approved application immediately.'
            );
        }
    }
}
