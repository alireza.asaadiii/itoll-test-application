<?php

namespace App\Services\Builder;

use App\Models\Application;
use App\Models\ApplicationStatus;
use App\Services\ApplicationStatusCreateService;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ApplicationCancelBuilder implements ApplicationCancelInterface
{
    public function __construct(public Application $application){}

    /**
     * @throws AuthorizationException
     */
    public function checkApplicationOwnership(): void
    {
        if ($this->application->complex_id != auth()->user()->complex_id) {
            throw new AuthorizationException();
        }
    }

    public function checkLastStatus(): void
    {
        $lastStatusId = $this->application->lastStatus->status_id;
        if ($lastStatusId != ApplicationStatus::STATUS['pending'] AND
            $lastStatusId != ApplicationStatus::STATUS['approved']
        ) {
            throw new UnprocessableEntityHttpException();
        }
    }

    public function applicationStatusCreate(): void
    {
        (new ApplicationStatusCreateService)->generate(
            $this->application,
            ['status_id' => ApplicationStatus::STATUS['canceled']]
        );
    }

    public function sendSpecificMessage(): array
    {
        return [
            'message' => 'Your service has been canceled successfully.'
        ];
    }
}
