<?php

namespace App\Services\Builder;

class ApplicationCancelDirector
{
    public static function build(ApplicationCancelInterface $builder): array
    {
        $builder->checkApplicationOwnership();
        $builder->checkLastStatus();
        $builder->applicationStatusCreate();

        return $builder->sendSpecificMessage();
    }
}
