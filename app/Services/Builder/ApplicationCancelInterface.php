<?php

namespace App\Services\Builder;

interface ApplicationCancelInterface
{
    public function checkApplicationOwnership(): void;
    public function checkLastStatus(): void;
    public function applicationStatusCreate(): void;
    public function sendSpecificMessage(): array;
}
