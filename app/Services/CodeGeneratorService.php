<?php

namespace App\Services;

class CodeGeneratorService
{
    public static function generate(): int
    {
        return fake()->randomNumber(9, true);
    }
}
