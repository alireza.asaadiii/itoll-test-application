<?php

namespace App\Services;

use Illuminate\Support\Facades\App;
use App\Events\ApplicationChangeStatusEvent;
use App\Repositories\DeliveryUserRepository;
use Illuminate\Contracts\Auth\Authenticatable;

class DeliveryUserUpdateService
{
    public function generate($data): Authenticatable
    {
        App::make(DeliveryUserRepository::class)->update($data);
        ApplicationChangeStatusEvent::dispatch();

        return auth()->user();
    }
}
