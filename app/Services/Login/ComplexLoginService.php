<?php

namespace App\Services\Login;

use App\Models\User;
use Illuminate\Auth\AuthenticationException;

class ComplexLoginService
{
    /**
     * @throws AuthenticationException
     */
    public function generate(array $loginData)
    {
        $user = User::where('email', $loginData['email'])->firstOrFail();
        return (new LoginService())->generate($user, $loginData);
    }
}
