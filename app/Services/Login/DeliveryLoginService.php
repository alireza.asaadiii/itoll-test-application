<?php

namespace App\Services\Login;

use App\Models\DeliveryUser;
use Illuminate\Auth\AuthenticationException;

class DeliveryLoginService
{
    /**
     * @throws AuthenticationException
     */
    public function generate(array $loginData)
    {
        $user = DeliveryUser::where('email', $loginData['email'])->firstOrFail();
        return (new LoginService())->generate($user, $loginData);
    }
}
