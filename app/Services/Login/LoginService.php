<?php

namespace App\Services\Login;

use App\Services\RoleService;
use App\Interfaces\UserInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\AuthenticationException;

class LoginService
{
    public function generate(UserInterface $user, array $loginData): array
    {
        if (! Hash::check($loginData['password'], $user->password)) {
            throw new AuthenticationException();
        }

        $permissions = RoleService::fetchPermissionsBasedOnRule($user);

        return ['token' => $user->createToken('api', $permissions)->plainTextToken];
    }
}
