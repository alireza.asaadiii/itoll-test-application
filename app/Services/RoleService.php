<?php

namespace App\Services;

use App\Models\{DeliveryUser, User};

class RoleService
{
    public static function fetchPermissionsBasedOnRule($user): array
    {
        return match (true) {
            $user instanceof User => self::fetchUserPermissions(),
            $user instanceof DeliveryUser => self::fetchDeliveryUserPermissions(),
        };
    }

    private static function fetchUserPermissions(): array
    {
        return [
            'application-create',
            'application-cancel',
            'application-detail'
        ];
    }

    private static function fetchDeliveryUserPermissions(): array
    {
        return [
            'application-list',
            'application-approved',
            'application-change-status',
            'update-location'
        ];
    }
}
