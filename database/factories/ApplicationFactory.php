<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code' => fake()->randomNumber(6, true),
            'source_location_longitude' => fake()->randomNumber(5, true),
            'source_location_latitude' => fake()->randomNumber(5, true),
            'source_address' => fake()->name(),
            'source_name' => fake()->name(),
            'source_phone_number' => fake()->randomNumber(9, true),
            'destination_location_longitude' => fake()->randomNumber(5, true),
            'destination_location_latitude' => fake()->randomNumber(5, true),
            'destination_address' => fake()->name(),
            'destination_name' => fake()->name(),
            'destination_phone_number' => fake()->randomNumber(9, true),
        ];
    }
}
