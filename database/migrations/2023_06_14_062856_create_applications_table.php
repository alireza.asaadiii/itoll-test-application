<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();

            $table->string('code');

            $table->unsignedBigInteger('creator_id')->unsigned()->index();
            $table->foreign('creator_id')->references('id')->on('users');

            $table->unsignedBigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('delivery_users');

            $table->unsignedBigInteger('complex_id')->unsigned()->index();
            $table->foreign('complex_id')->references('id')->on('complexes');

            $table->string('source_location_longitude');
            $table->string('source_location_latitude');
            $table->string('source_address');
            $table->string('source_name');
            $table->string('source_phone_number');

            $table->string('destination_location_longitude');
            $table->string('destination_location_latitude');
            $table->string('destination_address');
            $table->string('destination_name');
            $table->string('destination_phone_number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('applications');
    }
};
