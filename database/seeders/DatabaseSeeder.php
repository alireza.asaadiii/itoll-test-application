<?php

namespace Database\Seeders;

use App\Models\Complex;
use App\Models\DeliveryUser;
use App\Models\Delivery;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Complex::factory()->create([
            'name' => 'complex1',
            'url' => 'complex1.com'
        ]);

        User::factory()->create([
            'name' => 'complex1',
            'email' => 'complex1@complex.com',
            'password' => Hash::make(123456),
            'complex_id' => 1,

        ]);

        DeliveryUser::factory()->create([
            'name' => 'delivery1',
            'email' => 'delivery1@delivery.com',
            'password' => Hash::make(123456),
        ]);

        Status::insert([
            ['name' => 'pending', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'canceled_by_complex', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'approved', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'received', 'created_at' => now(), 'updated_at' => now()],
            ['name' => 'done', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
