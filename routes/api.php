<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{AuthController, ApplicationController, DeliveryUserController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('/v1/auth')->name('auth.')->group( function () {
    Route::post('/complex-login',[AuthController::class, 'complexLogin']);
    Route::post('/delivery-login',[AuthController::class, 'deliveryLogin']);

    Route::put('/',[DeliveryUserController::class, 'update'])
        ->middleware('auth:sanctum')
        ->middleware('abilities:update-location');
});

Route::prefix('/v1/applications')->middleware('auth:sanctum')
    ->name('applications.')->group( function () {

        Route::post('/',[ApplicationController::class, 'create'])
            ->middleware('abilities:application-create');

        Route::get('/{application}',[ApplicationController::class, 'detail'])
            ->middleware('abilities:application-detail');

        Route::get('/',[ApplicationController::class, 'list'])
            ->middleware('abilities:application-list');

        Route::put('/{application}/cancel',[ApplicationController::class, 'cancelByComplex'])
            ->middleware('abilities:application-cancel');

        Route::put('/{application}/approved',[ApplicationController::class, 'approved'])
            ->middleware('abilities:application-approved');

        Route::put('/{application}/change-status',[ApplicationController::class, 'changeStatus'])
            ->middleware('abilities:application-change-status');
    });
