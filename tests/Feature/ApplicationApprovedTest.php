<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Application;
use App\Models\DeliveryUser;
use Laravel\Sanctum\Sanctum;
use App\Models\ApplicationStatus;
use Illuminate\Http\Response as HttpResponse;

class ApplicationApprovedTest extends TestCase
{
    public function test_application_approved_without_permission(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            []
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/approved');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_application_approved(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        ApplicationStatus::factory()->create([
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['pending']
        ]);

        Sanctum::actingAs(
            DeliveryUser::factory()->create(),
            ['application-approved']
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/approved');

        $response->assertStatus(HttpResponse::HTTP_OK);
    }

    public function test_application_approved_database(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        ApplicationStatus::factory()->create([
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['pending']
        ]);

        $deliveryUser = DeliveryUser::factory()->create();

        Sanctum::actingAs(
            $deliveryUser,
            ['application-approved']
        );

        $this->put('/api/v1/applications/' . $application->id . '/approved');

        $this->assertDatabaseHas('applications', [
            'user_id' => $deliveryUser->id
        ]);
    }

    public function test_application_status_approved_database(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        ApplicationStatus::factory()->create([
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['pending']
        ]);

        $deliveryUser = DeliveryUser::factory()->create();

        Sanctum::actingAs(
            $deliveryUser,
            ['application-approved']
        );

        $this->put('/api/v1/applications/' . $application->id . '/approved');

        $this->assertDatabaseHas('application_statuses', [
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['approved']
        ]);
    }
}
