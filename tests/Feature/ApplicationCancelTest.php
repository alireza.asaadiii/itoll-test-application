<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Complex;
use App\Models\Application;
use Laravel\Sanctum\Sanctum;
use App\Models\ApplicationStatus;
use Illuminate\Http\Response as HttpResponse;

class ApplicationCancelTest extends TestCase
{
    public function test_application_cancel_without_permission(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            []
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/cancel');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_application_cancel_ownership(): void
    {
        $user = User::factory()->create();

        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        ApplicationStatus::factory()->create([
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['pending']
        ]);

        Sanctum::actingAs(
            User::factory()->create([
                'complex_id' => Complex::factory()->create()
            ]),
            ['application-cancel']
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/cancel');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_application_cancel_last_status(): void
    {
        $user = User::factory()->create();

        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        ApplicationStatus::factory()->create([
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['canceled']
        ]);

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-cancel']
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/cancel');

        $response->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
