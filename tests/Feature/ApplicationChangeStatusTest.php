<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Application;
use Laravel\Sanctum\Sanctum;
use App\Models\ApplicationStatus;
use Illuminate\Http\Response as HttpResponse;

class ApplicationChangeStatusTest extends TestCase
{
    public function test_application_change_status_without_permission(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            []
        );

        $response = $this->put('/api/v1/applications/' . $application->id . '/change-status');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_application_change_status_with_incorrect_status(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-change-status']
        );

        $applicationChangeStatusData = [
            'status_id' => ApplicationStatus::STATUS['approved'],
            'delivery_location_longitude' => fake()->randomNumber(6, true),
            'delivery_location_latitude' => fake()->randomNumber(6, true),
        ];

        $response = $this->put('/api/v1/applications/' . $application->id . '/change-status', $applicationChangeStatusData);

        $response->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_application_change_status(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-change-status']
        );

        $applicationChangeStatusData = [
            'status_id' => ApplicationStatus::STATUS['received'],
            'delivery_location_longitude' => fake()->randomNumber(6, true),
            'delivery_location_latitude' => fake()->randomNumber(6, true),
        ];

        $response = $this->put('/api/v1/applications/' . $application->id . '/change-status', $applicationChangeStatusData);
        $response->assertValid();

        $response->assertStatus(HttpResponse::HTTP_OK);
    }

    public function test_application_change_status_database(): void
    {
        $user = User::factory()->create();
        $application = Application::factory([
            'creator_id' => $user->id,
            'complex_id' => $user->complex_id
        ])->create();

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-change-status']
        );

        $applicationChangeStatusData = [
            'status_id' => ApplicationStatus::STATUS['received'],
            'delivery_location_longitude' => fake()->randomNumber(6, true),
            'delivery_location_latitude' => fake()->randomNumber(6, true),
        ];

        $this->put('/api/v1/applications/' . $application->id . '/change-status', $applicationChangeStatusData);
        $this->assertDatabaseHas('application_statuses', [
            'application_id' => $application->id,
            'status_id' => ApplicationStatus::STATUS['received']
        ]);
    }
}
