<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use App\Models\ApplicationStatus;
use Illuminate\Http\Response as HttpResponse;
use App\Http\Requests\ApplicationCreateRequest;

class ApplicationCreateTest extends TestCase
{
    public function test_application_create_without_permission(): void
    {
        Sanctum::actingAs(
            User::factory()->create(),
            []
        );

        $response = $this->get('/api/v1/applications');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_application_create(): void
    {
        $request = new ApplicationCreateRequest();
        $request->merge([
            'source_location_longitude' => (string)fake()->randomNumber(5, true),
            'source_location_latitude' => (string)fake()->randomNumber(5, true),
            'source_address' => fake()->name(),
            'source_name' => fake()->name(),
            'source_phone_number' => (string)fake()->randomNumber(9, true),
            'destination_location_longitude' => (string)fake()->randomNumber(5, true),
            'destination_location_latitude' => (string)fake()->randomNumber(5, true),
            'destination_address' => fake()->name(),
            'destination_name' => fake()->name(),
            'destination_phone_number' => (string)fake()->randomNumber(9, true)
        ]);

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-create']
        );

        $response = $this->post('/api/v1/applications', $request->input());

        $response->assertStatus(HttpResponse::HTTP_CREATED);
    }

    public function test_application_create_database(): void
    {
        $request = new ApplicationCreateRequest();
        $request->merge([
            'source_location_longitude' => fake()->randomNumber(5, true),
            'source_location_latitude' => fake()->randomNumber(5, true),
            'source_address' => fake()->name(),
            'source_name' => fake()->name(),
            'source_phone_number' => fake()->randomNumber(9, true),
            'destination_location_longitude' => fake()->randomNumber(5, true),
            'destination_location_latitude' => fake()->randomNumber(5, true),
            'destination_address' => fake()->name(),
            'destination_name' => fake()->name(),
            'destination_phone_number' => fake()->randomNumber(9, true)
        ]);

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-create']
        );

        $response = $this->post('/api/v1/applications', $request->input());

        $this->assertDatabaseHas('applications', [
            'code' => $response['data']['code']
        ]);
    }

    public function test_application_status_create_database(): void
    {
        $request = new ApplicationCreateRequest();
        $request->merge([
            'source_location_longitude' => (string)fake()->randomNumber(5, true),
            'source_location_latitude' => (string)fake()->randomNumber(5, true),
            'source_address' => fake()->name(),
            'source_name' => fake()->name(),
            'source_phone_number' => (string)fake()->randomNumber(9, true),
            'destination_location_longitude' => (string)fake()->randomNumber(5, true),
            'destination_location_latitude' => (string)fake()->randomNumber(5, true),
            'destination_address' => fake()->name(),
            'destination_name' => fake()->name(),
            'destination_phone_number' => (string)fake()->randomNumber(9, true)
        ]);

        Sanctum::actingAs(
            User::factory()->create(),
            ['application-create']
        );

        $response = $this->post('/api/v1/applications', $request->input());

        $this->assertDatabaseHas('application_statuses', [
            'application_id' => $response['data']['id'],
            'status_id' => ApplicationStatus::STATUS['pending'],
        ]);
    }
}
