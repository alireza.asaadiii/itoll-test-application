<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class ComplexLoginTest extends TestCase
{
    public function test_login_with_incorrect_email(): void
    {
        $user = User::factory()->create();
        $requestData = [
            'email' => 'alireza.123@gmail.com',
            'password' => 'password'
        ];

        $response = $this->post('/api/v1/auth/complex-login', $requestData);

        $response->assertNotFound();
    }

    public function test_login_with_incorrect_password(): void
    {
        $user = User::factory()->create();
        $requestData = [
            'email' => $user->email,
            'password' => 'password123'
        ];

        $response = $this->post('/api/v1/auth/complex-login', $requestData);

        $response->assertUnauthorized();
    }

    public function test_login(): void
    {
        $user = User::factory()->create();
        $requestData = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $response = $this->post('/api/v1/auth/complex-login', $requestData);

        $response->assertCreated();
    }
}
