<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\DeliveryUser;

class DeliveryLoginTest extends TestCase
{
    public function test_login_with_incorrect_email(): void
    {
        $user = DeliveryUser::factory()->create();
        $requestData = [
            'email' => 'alireza.123@gmail.com',
            'password' => 'password'
        ];

        $response = $this->post('/api/v1/auth/delivery-login', $requestData);

        $response->assertNotFound();
    }

    public function test_login_with_incorrect_password(): void
    {
        $user = DeliveryUser::factory()->create();
        $requestData = [
            'email' => $user->email,
            'password' => 'password123'
        ];

        $response = $this->post('/api/v1/auth/delivery-login', $requestData);

        $response->assertUnauthorized();
    }

    public function test_login(): void
    {
        $user = DeliveryUser::factory()->create();
        $requestData = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $response = $this->post('/api/v1/auth/delivery-login', $requestData);

        $response->assertCreated();
    }
}
