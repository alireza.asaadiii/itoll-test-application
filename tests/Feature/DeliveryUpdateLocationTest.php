<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\DeliveryUser;
use Laravel\Sanctum\Sanctum;
use Illuminate\Support\Facades\Event;
use App\Events\ApplicationChangeStatusEvent;
use Illuminate\Http\Response as HttpResponse;

class DeliveryUpdateLocationTest extends TestCase
{
    public function test_update_delivery_user_without_permission(): void
    {
        $user = DeliveryUser::factory()->create();

        Sanctum::actingAs(
            $user,
            []
        );

        $response = $this->put('/api/v1/auth');

        $response->assertStatus(HttpResponse::HTTP_FORBIDDEN);
    }

    public function test_update_delivery_user(): void
    {
        $user = DeliveryUser::factory()->create();

        Sanctum::actingAs(
            $user,
            ['update-location']
        );

        $userUpdateRequest = [
            'delivery_location_longitude' => fake()->randomNumber(6, true),
            'delivery_location_latitude' => fake()->randomNumber(6, true),
        ];

        Event::fake();
        $response = $this->put('/api/v1/auth', $userUpdateRequest);
        Event::assertDispatched(ApplicationChangeStatusEvent::class);
        $response->assertStatus(HttpResponse::HTTP_OK);
    }

    public function test_update_delivery_user_database(): void
    {
        $user = DeliveryUser::factory()->create();

        Sanctum::actingAs(
           $user,
            ['update-location']
        );

        $userUpdateRequest = [
            'delivery_location_longitude' => fake()->randomNumber(6, true),
            'delivery_location_latitude' => fake()->randomNumber(6, true),
        ];

        $this->put('/api/v1/auth', $userUpdateRequest);

        $this->assertDatabaseHas('delivery_users', $userUpdateRequest);
    }
}
